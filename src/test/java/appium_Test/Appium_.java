package appium_Test;

import actions.appiumSteps.EricBankLoginActions;
import core_Base.AppiumDriverFactory;
import core_Base.BaseClass;
import core_Base.extentReport.Reporting;
import org.junit.jupiter.api.*;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.net.MalformedURLException;

public class Appium_ extends AppiumDriverFactory {

    static DesiredCapabilities caps;

    public Appium_() throws MalformedURLException {
        super(caps);
    }

    @BeforeAll
    public static void setUp(TestInfo testInfo) throws MalformedURLException {
        TESTNAME = testInfo.getDisplayName();
        Reporting.createTest();

        caps = new DesiredCapabilities();
        caps = AppiumDriverFactory.setDesiredCapabilites("192.168.117.101", "5.0", "Android",
                "com.experitest.ExperiBank", ".LoginActivity",
                BaseClass.getSystemProperty()+"/src/main/resources/appium_APKs/EriBank.apk", "uiautomator2");
        appiumDriverFactoryInstance = new AppiumDriverFactory(caps);

    }



    @AfterAll
    public static void stopServer() {
        try {
            AppiumDriverFactory.getAndroidDriver().closeApp();
            //AppiumDriverFactory.getAndroidDriver().close();
            AppiumDriverFactory.stopAppiumServerInstance();
            System.out.println("Server is running: " + AppiumDriverFactory.getAppiumDriverLocalService().isRunning());
        } catch (Exception ex) {
            System.err.println("[Unable to close App] - Check the server");
        }

    }

    @Test
    public void startUpTest() throws InterruptedException, IOException {
        getAndroidDriver().launchApp();
        String result = EricBankLoginActions.login("testUsername","testPassword");
     //   Assertions.assertTrue(result!=null);
    }
}
