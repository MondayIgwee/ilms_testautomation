package stepDefinition;

import actions.passwordManager_Steps.PasswordMainView_steps;
import actions.passwordManager_Steps.Pro_LogOut_steps;
import core_Base.BaseClass;
import core_Base.extentReport.Reporting;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.*;
import actions.passwordManager_Steps.Pro_Login_steps;
import org.openqa.selenium.WebDriver;

import java.io.IOException;


public class IndividualLife extends BaseClass {

    Scenario scenario;

    public IndividualLife() {
        super(BrowserType.CHROME,webDriver);
    }


    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
        TestName = scenario.getName();
        Reporting.createTest();
    }


    @Given("user password manager url")
    public void userPasswordManagerUrl() throws IOException, InterruptedException {
        Pro_Login_steps.openPasswordManagerUrl();
    }

    @Given("user enters username and password")
    public void userEntersUsernameAndPassword() {
    }

    @And("user select logOnt options")
    public void userSelectLogOntOptions() {
    }

    @And("click login button")
    public void clickLoginButton() {
    }

    @Then("user should be successfully logged in")
    public void userShouldBeSuccessfullyLoggedIn() throws IOException, InterruptedException {
        Pro_LogOut_steps.logonProManager();
    }
//*******************************************************

    @Given("user click connection link")
    public void userClickConnectionLink() throws IOException, InterruptedException {
        PasswordMainView_steps.connectToVirtualMachineProManager();
    }

    @And("user click virtual machine")
    public void userClickVirtualMachine() {
    }


    @After
    public void tearDown() throws InterruptedException, IOException {

        BaseClass.cleanUp();
    }
}
