package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features ="src/test/resources/features/individualLifeCover",
        glue = {"stepDefinition"},
        tags = "@sanity",
        monochrome = true
)
public class Runner {

    }
