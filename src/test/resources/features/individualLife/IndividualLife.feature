@sanity
Feature: Logon to Pro Password Manager

  Background: As a User, i want to apply for individual life cover
    Given user password manager url

    Scenario: Details - Enter Credentials
      Given user enters username and password
      And user select logOnt options
      And click login button
      Then user should be successfully logged in

  Scenario: Details - Connect to a Virtual Machine
    Given user click connection link
    And user click virtual machine
