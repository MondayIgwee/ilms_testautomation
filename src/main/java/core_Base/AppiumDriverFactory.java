package core_Base;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import tools.CommonLibsUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AppiumDriverFactory {

    public static AppiumDriverFactory appiumDriverFactoryInstance;
    private static AndroidDriver<WebElement> androidDriver;
    private static AppiumDriverLocalService appiumDriverLocalService;
    private static int maxTries;
    public static String TESTNAME;
    static String nodePath = "C:\\Program Files\\nodejs\\node.exe";


    public AppiumDriverFactory(DesiredCapabilities caps) throws MalformedURLException {
        startServer();
        startDriver(caps);
        maxTries = 10;
    }


    /**
     * Start Server
     */
    public void startServer() {
        appiumDriverLocalService = AppiumDriverLocalService.buildDefaultService();
        appiumDriverLocalService.start();
        System.out.println("Server is running: " + appiumDriverLocalService.isRunning() + "");
    }

    public static AppiumDriverLocalService getAppiumDriverLocalService() {
        return appiumDriverLocalService;
    }

    /**
     * Stop Server
     */
    public static void stopAppiumServerInstance() {
        Runtime runtime = Runtime.getRuntime();
        try {
            String[] command = {nodePath, "/F", "/IM", "node.exe"};
            runtime.exec(command);
            System.out.println("[Server Successfully Stopped]");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create and Start Appium Driver
     */

    public void startDriver(DesiredCapabilities caps) {
        try {
            androidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
        } catch (MalformedURLException ex) {
            System.err.println("[FAILED] - Server [ERROR] Failed to Start Appium Server, Please check Appium Server");
        }
    }


    /**
     * Create Capabilities
     */

    public static DesiredCapabilities setDesiredCapabilites(String deviceName, String platformVersion, String platformName,
                                                            String appPackage, String appActivity, String appFilePath, String automationName) {

        DesiredCapabilities caps = new DesiredCapabilities();

        caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, automationName);
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
        caps.setCapability("appPackage", appPackage);
        caps.setCapability("appActivity", appActivity);
        caps.setCapability("fullReset", true);
        caps.setCapability("app", appFilePath);
        caps.setCapability("sendKeyStrategy", "grouped");
        return caps;
    }


    /**
     * Create Appium Methods
     */

    public static AndroidDriver getAndroidDriver() {
        return androidDriver;
    }


    public static boolean waitForElement(By selector) {
        boolean found = false;
        int counter = 0;
        try {
            while (!found && counter < 30) {
                try {
                    WebDriverWait wait = new WebDriverWait(getAndroidDriver(), 50);
                    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(selector));
                    found = true;
                } catch (WebDriverException we) {
                    counter++;
                    CommonLibsUtils.pause(2000);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean clickElement(By selector) throws InterruptedException {
        try {
            WebDriverWait wait = new WebDriverWait(getAndroidDriver(), 5);
            wait.until(ExpectedConditions.elementToBeClickable(selector));
            AndroidElement elementToClick = (AndroidElement) getAndroidDriver().findElement(selector);
            elementToClick.click();
        } catch (WebDriverException we) {
            CommonLibsUtils.pause(2000);
        }
        return true;
    }

    public static boolean clickElementByAccessibilityId(String selector) throws InterruptedException {
        try {
            AndroidElement elementToClick = (AndroidElement) getAndroidDriver().findElementByAccessibilityId(selector);
            elementToClick.click();
        } catch (WebDriverException we) {
            CommonLibsUtils.pause(2000);
        }
        return true;
    }


    public static boolean enterText(By selector, String textEnter) {
        try {
            WebDriverWait wait = new WebDriverWait(getAndroidDriver(), 5);
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
            AndroidElement elementText = (AndroidElement) getAndroidDriver().findElement(selector);
            elementText.sendKeys(textEnter);
            return true;
        } catch (WebDriverException we) {
            return false;
        }

    }

    public static String getText(By selector, String textEnter) {
        try {
            WebDriverWait wait = new WebDriverWait(getAndroidDriver(), 50);
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
            WebElement elementToClick = getAndroidDriver().findElement(selector);
            return elementToClick.getText();
        } catch (WebDriverException we) {
            return "";
        }
    }


    public static boolean validateElementText(By selector, String textToValidate) {
        try {
            WebDriverWait wait = new WebDriverWait(getAndroidDriver(), 50);
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
            WebElement elementToClick = getAndroidDriver().findElement(selector);
            elementToClick.getText().equalsIgnoreCase(textToValidate);
            System.out.println("text To Validate " + textToValidate);
            return true;
        } catch (WebDriverException we) {
            return false;
        }
    }

    public static Object findEditText(String text) throws InterruptedException {
        int attempts = 0;

        try {
            AndroidElement elements = (AndroidElement) getAndroidDriver().findElementsByXPath("//android.widget.EditText");
            return elements.findElementsByXPath(text).equals(text);
        } catch (StaleElementReferenceException e) {

            if(attempts > 10){
                return false;
            }
            CommonLibsUtils.pause(1000);
            attempts++;
            return text;
        }
    }

    public static boolean click(By selector) throws InterruptedException {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getAndroidDriver())
                .withTimeout(60, TimeUnit.SECONDS)
                .pollingEvery(60, TimeUnit.SECONDS);

        int count = 0;

        while (true) {

            try {
                AndroidElement elementToClick = (AndroidElement) getAndroidDriver().findElement(selector);
                elementToClick.click();
                CommonLibsUtils.pause(1000);
                break;

            } catch (InvalidElementStateException e) {
                if (++count == maxTries) throw e;
            } catch (StaleElementReferenceException e) {
                if (++count == maxTries) throw e;
            } catch (java.util.NoSuchElementException e) {
                if (++count == maxTries) throw e;
            } catch (WebDriverException e) {
                CommonLibsUtils.pause(1000);
                if (++count == maxTries) throw e;
            }
        }
        return false;
    }

    //******************************************************

}
