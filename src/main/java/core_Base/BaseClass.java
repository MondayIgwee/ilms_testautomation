package core_Base;

import org.junit.Assert;;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import tools.CommonLibsUtils;
import tools.ConsoleColors;
import tools.Env_PropertiesUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BaseClass extends SeleniumDriverFactory implements Runnable{

    public static String TestName;
    private static String reportingDirectory;
    public static int screenShotCounter;
    public static int pageLoadTimeout;
    public static BaseClass baseClass = new BaseClass(BrowserType.CHROME,webDriver);

    public BaseClass(BrowserType browserType, WebDriver driver) {
        super(browserType,driver);
        pageLoadTimeout=50;

    }


    public static String init() throws IOException, InterruptedException {
        try {
            baseClass.launchDriver();
            Assert.assertTrue("Driver Not Loaded! Please Verify is loaded", baseClass.getWebDriver() != null);
            Env_PropertiesUtils.getPropValues();

            baseClass.getWebDriver().get(Env_PropertiesUtils.getPassTrixMain());
            baseClass.getWebDriver().manage().timeouts().pageLoadTimeout(pageLoadTimeout, TimeUnit.SECONDS);
        }catch (WebDriverException we){
            System.err.println(we.getMessage());
        }
        return "";
    }


    public static boolean cleanUp() throws InterruptedException {
        CommonLibsUtils.pause(3000);
        try {
            baseClass.getWebDriver().manage().deleteAllCookies();
            baseClass.getWebDriver().close();
            baseClass.getWebDriver().quit();
            return true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    public static String getReportingDirectory() {
        return reportingDirectory;
    }

    public static void setReportingDirectory(String reportingDirectory) {
        BaseClass.reportingDirectory = reportingDirectory;
    }

    public static String getSystemProperty() {
        return System.getProperty("user.dir");
    }

    @Override
    public void run() {
        for (int thread=0;thread<10;thread++){
            int x = thread;
            System.out.println(ConsoleColors.TEXT_BLUE +"My Threads in BaseClass Object: "+x);
        }
    }
}
