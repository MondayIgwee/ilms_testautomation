package core_Base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.*;
import tools.Chrome_OptionsUtils;
import tools.CommonLibsUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class SeleniumDriverFactory {

    public static WebDriver webDriver;
    public enum BrowserType {CHROME, IE, FIREFOX, SAFARI}

    private BrowserType currentBrowser;
    public static int maxTries = 10;

    public SeleniumDriverFactory(BrowserType browserType,WebDriver driver) {
        currentBrowser = browserType;
        this.webDriver=driver;
    }

    public boolean launchDriver() {
        switch (this.currentBrowser) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
                this.webDriver = new ChromeDriver(Chrome_OptionsUtils.getOptions());
                SessionId sessionId = ((ChromeDriver) webDriver).getSessionId();
                System.out.println("Chrome sessionID: " + sessionId.toString());
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                this.webDriver = new FirefoxDriver();
                break;
            case IE:
                WebDriverManager.edgedriver();
                this.webDriver = new InternetExplorerDriver();
                break;
            case SAFARI:
                WebDriverManager.edgedriver();
                this.webDriver = new SafariDriver();
                break;
            default:
                System.out.println("Driver Not found! Please verify the driver");
                break;
        }
        this.webDriver.manage().window().maximize();
        return true;
    }


    /**
     * Create WebDriver Methods
     */

    public BrowserType getCurrentBrowser() {
        return currentBrowser;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public boolean waitForElement(By selector) {
        boolean found = false;
        int counter = 0;
        try {
            while (!found && counter < 30) {
                try {
                    WebDriverWait wait = new WebDriverWait(this.getWebDriver(), 50);
                    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(selector));
                    found = true;
                } catch (WebDriverException we) {
                    counter++;
                    CommonLibsUtils.pause(2000);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean clickElement(By selector) throws InterruptedException {
        try {
            WebDriverWait wait = new WebDriverWait(this.getWebDriver(), 50);
            wait.until(ExpectedConditions.elementToBeClickable(selector));
            WebElement elementToClick = this.getWebDriver().findElement(selector);
            elementToClick.click();
        } catch (WebDriverException we) {
            CommonLibsUtils.pause(2000);
        }
        return true;
    }


    public boolean enterText(By selector, String textEnter) throws InterruptedException {
        try {
            WebDriverWait wait = new WebDriverWait(this.getWebDriver(), 50);
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));

            WebElement elementToClick = this.getWebDriver().findElement(selector);
            elementToClick.sendKeys(textEnter);
            return true;
        } catch (WebDriverException we) {
            return false;
        }

    }

    public String getText(By selector) throws InterruptedException {
        try {
            WebDriverWait wait = new WebDriverWait(this.getWebDriver(), 50);
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
            WebElement elementToClick = this.getWebDriver().findElement(selector);
            return elementToClick.getText();
        } catch (WebDriverException we) {
            return "";
        }
    }


    public boolean validateElementText(By selector, String textToValidate) throws InterruptedException {
        try {
            WebDriverWait wait = new WebDriverWait(this.getWebDriver(), 50);
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
            WebElement elementToClick = this.getWebDriver().findElement(selector);
            elementToClick.getText().equalsIgnoreCase(textToValidate);
            System.out.println("text To Validate " + textToValidate);
            return true;
        } catch (WebDriverException we) {
            return false;
        }

    }

    public boolean navigate(String url) throws InterruptedException {
        try {
            this.getWebDriver().navigate().to(new URL(url));
            return true;
        } catch (WebDriverException | MalformedURLException we) {
            return false;
        }
    }


    public void clickWebElement(WebElement element) throws InterruptedException {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getWebDriver())
                .withTimeout(60, TimeUnit.SECONDS)
                .pollingEvery(60, TimeUnit.SECONDS);

        int count = 0;

        while (true) {

            try {
//                    element = wait.until( ExpectedConditions.visibilityOf(element));
//                    element = wait.until(ExpectedConditions.elementToBeClickable(element));
              //  ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].click();", element);
                element.click();
                CommonLibsUtils.pause(1000);
                break;
            } catch (InvalidElementStateException e) {
                CommonLibsUtils.pause(1000);
                if (++count == maxTries) throw e;
            } catch (StaleElementReferenceException e) {
                System.out.println("Stale :" + count);
                CommonLibsUtils.pause(1000);
                if (++count == maxTries) throw e;
            } catch (java.util.NoSuchElementException e) {
                CommonLibsUtils.pause(1000);
                if (++count == maxTries) throw e;
            } catch (WebDriverException e) {
                CommonLibsUtils.pause(1000);
                if (++count == maxTries) throw e;
            }
        }
    }
    public void sendKeys(WebElement element, String text) throws InterruptedException {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver)
                .withTimeout(1, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS);

        int count = 0;

        while (true) {
            try {
                element = wait.until( ExpectedConditions.visibilityOf(element));
                element = wait.until( ExpectedConditions.elementToBeClickable(element));
                element.clear();
                ((JavascriptExecutor) webDriver).executeScript("arguments[1].value = arguments[0];", text,element);
                break;
            }catch (InvalidElementStateException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            }catch (StaleElementReferenceException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            }catch (java.util.NoSuchElementException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            }catch (WebDriverException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            }
        }

    }
    public void Select(WebElement element, String text) throws InterruptedException {
        Wait<WebDriver> wait = new FluentWait<WebDriver>( getWebDriver() )
                .withTimeout(1, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS);

        int count = 0;
        WebElement we;
        WebElement we2;

        while (true) {
            try {
                we = wait.until( ExpectedConditions.elementToBeClickable( element ) );
                we2 = wait.until( ExpectedConditions.elementToBeClickable( element ) );
                Select dropdown = new Select(element);
                dropdown.selectByVisibleText(text);
                break;
            }catch (InvalidElementStateException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            }catch (StaleElementReferenceException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            }catch (java.util.NoSuchElementException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            }catch (WebDriverException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            }
        }
    }

}
