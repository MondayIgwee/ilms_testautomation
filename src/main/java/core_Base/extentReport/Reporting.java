package core_Base.extentReport;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import core_Base.BaseClass;
import tools.TakeScreenshotsUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Reporting extends BaseClass {

    private static ExtentReports report;
    private static ExtentTest currentTest;
    private static ExtentHtmlReporter html;

    public Reporting(BrowserType browserType) {
        super(browserType,webDriver);
    }


    public static void setup() {
        setReportingDirectory(getSystemProperty() + "/Test_Reports/" + TestName + "/" + getCurTime() + "/");
        new File(getReportingDirectory()).mkdir();
        report = new ExtentReports();
        html = new ExtentHtmlReporter(getReportingDirectory() + "ExtentReport.html");
        report.attachReporter(html);
        report.setAnalysisStrategy(AnalysisStrategy.TEST);
        report.flush();
    }

    public static void createTest() {
        try {
            if (report == null) setup();
            if (currentTest == null || !currentTest.getModel().getName().equals(TestName))
                currentTest = report.createTest(TestName);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void stepPassed(String message) {
        if (currentTest == null) createTest();
        currentTest.pass(message);
        System.out.println("[SUCCESS] - " + message);
        report.flush();
    }

    public static void info(String message) {
        if (currentTest == null) createTest();
        currentTest.info(message);
        System.out.println("[INFO] - " + message);
        report.flush();
    }

    public static void warning(String message) {
        if (currentTest == null) createTest();
        currentTest.warning(message);
        System.out.println("[WARNING] - " + message);
        report.flush();
    }

    public static void stepPassedWithScreenShot(String message) throws IOException {
        if (currentTest == null) createTest();
        try {
            currentTest.pass(message,MediaEntityBuilder.createScreenCaptureFromPath(TakeScreenshotsUtils.takeScreenshots(true)).build());
            System.out.println(message);
        } catch (Exception e) {
            currentTest.pass(message + "- screenshot capture failure");
        }
        report.flush();
    }


    public static String testFailed(String message) {
        if (currentTest == null) createTest();
        try {
            currentTest.fail(message, MediaEntityBuilder.createScreenCaptureFromPath(TakeScreenshotsUtils.takeScreenshots(false)).build());
        } catch (Exception e) {
            currentTest.fail(message + "- screenshot capture failure");
        }
        return message;
    }

    public static String finalisedTest() {
        if (currentTest == null) createTest();
        try {
            currentTest.pass("Test Complete", MediaEntityBuilder.createScreenCaptureFromPath(TakeScreenshotsUtils.takeScreenshots(true)).build());
        } catch (Exception e) {
            currentTest.pass("Test Completed");
        }
        System.out.println("[COMPLETE] - Test Complete!");
        report.flush();
        return null;
    }

    public static String getCurTime() {
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss");
        return ft.format(date);
    }
}
