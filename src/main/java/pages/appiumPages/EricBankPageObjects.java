package pages.appiumPages;

import org.openqa.selenium.By;

public class EricBankPageObjects {

    public static By username(){return By.id("usernameTextField");}
    public static By password(){return By.id("passwordTextField");}
    public static By login(){return By.id("loginButton");}
}
