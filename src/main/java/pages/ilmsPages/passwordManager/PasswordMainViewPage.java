package pages.ilmsPages.passwordManager;

import core_Base.BaseClass;
import core_Base.SeleniumDriverFactory;
import core_Base.extentReport.Reporting;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tools.CommonLibsUtils;
import tools.Env_PropertiesUtils;
import tools.WebElementUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class PasswordMainViewPage extends BaseClass {

    static CommonLibsUtils commonLibsUtils;


    public PasswordMainViewPage(BrowserType browserType, WebDriver driver) {
        super(browserType,driver);
        PageFactory.initElements(this.getWebDriver(), this);
    }


    //  @FindBy(xpath = "//*[@id='pmpmenu-ul']/li[2]") static WebElement connections;
    // @FindBy(xpath = "//*[@id='rdp_auto_0']") static WebElement IL_20ISPD_VirtualMachine;

    //static By connections = xpath("//*[@id='pmpmenu-ul']/li[2]");


    /**
     * Create Password View Methods
     */
    public static WebElement clickConnections() throws IOException {
        commonLibsUtils = new CommonLibsUtils(baseClass.getWebDriver());
        try {
            baseClass.getWebDriver().manage().timeouts().implicitlyWait(pageLoadTimeout, TimeUnit.SECONDS);
            WebElement connections = WebElementUtils.getElementLocator("xpath", "//*[@id='pmpmenu-ul']/li[2]");
            commonLibsUtils.scrollToElement(baseClass.getWebDriver(), connections);
            commonLibsUtils.javaScriptExecutor_HighlightElement(connections);
            return connections;
        } catch (NoSuchElementException no) {
            Reporting.testFailed("[FAILED] - failed to click!!");
            no.printStackTrace();
        }
        Reporting.stepPassedWithScreenShot("Click Connection");
        try {
            CommonLibsUtils.pause(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static WebElement clickVirtualMachine() throws IOException {
        commonLibsUtils = new CommonLibsUtils(baseClass.getWebDriver());
        try {
            baseClass.getWebDriver().manage().timeouts().implicitlyWait(pageLoadTimeout, TimeUnit.SECONDS);
            baseClass.getWebDriver().findElement(By.xpath("//*[@id='rdp_auto_0']")).click();
            WebElement IL_20ISPD_VirtualMachine = WebElementUtils.getElementLocator("linkText", "Choose domain account");
            commonLibsUtils.javaScriptExecutor_HighlightElement(IL_20ISPD_VirtualMachine);
            return IL_20ISPD_VirtualMachine;
        } catch (NoSuchElementException no) {
            Reporting.testFailed("[FAILED] - failed to click Virtual Machine");
            no.printStackTrace();
            try {
                CommonLibsUtils.pause(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Reporting.stepPassedWithScreenShot("Click Connection");

        return null;
    }

    public static WebElement autoLogonFullView() throws IOException {
        Env_PropertiesUtils.getPropValues();
        commonLibsUtils = new CommonLibsUtils(baseClass.getWebDriver());
        baseClass.getWebDriver().manage().timeouts().implicitlyWait(pageLoadTimeout, TimeUnit.SECONDS);

        try {

            WebElementUtils.getElementLocator("xpath", "//div/input[@id='logged_in_account']").click();
            CommonLibsUtils.pause(3000);

            WebElement password = WebElementUtils.getElementLocator("xpath", "//*[@id='login_password']");
            WebElement reason = WebElementUtils.getElementLocator("xpath", "//*[@id='autologon_reason']");
            baseClass.sendKeys(password, Env_PropertiesUtils.pass);
            baseClass.sendKeys(reason, "test");

            return WebElementUtils.getElementLocator("xpath", "//*[@id='CustomAutoLogonHelper_CT']/table[2]/tbody/tr[4]/td/button[1]");
        } catch (NoSuchElementException | InterruptedException no) {
            Reporting.testFailed("[FAILED] - failed to click Virtual Machine");
            no.printStackTrace();
        }
        Reporting.stepPassedWithScreenShot("Selected A Domain Account");
        return null;
    }

    public static void switchTabs() throws IOException {
        commonLibsUtils = new CommonLibsUtils(baseClass.getWebDriver());
        try {
            ArrayList tabs = new ArrayList(baseClass.getWebDriver().getWindowHandles());
            System.out.println(tabs.size());
            baseClass.getWebDriver().switchTo().window(tabs.get(1).toString());

            WebDriverWait wait = new WebDriverWait(baseClass.getWebDriver(),5);
            wait.until(ExpectedConditions.visibilityOf( WebElementUtils.getElementLocator("xpath","//table/tbody/tr[2]/td/input[@id='txtUsername']]")));

            commonLibsUtils.javaScriptExecutor_HighlightElement(WebElementUtils.getElementLocator("xpath","//table/tbody/tr[2]/td/input[@id='txtUsername']]"));


            Reporting.stepPassedWithScreenShot("Switched Tabs");
            try {
                CommonLibsUtils.pause(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (NoSuchElementException no) {
            Reporting.testFailed("[FAILED] - failed to click Connection");
            no.printStackTrace();
        }
    }
}
