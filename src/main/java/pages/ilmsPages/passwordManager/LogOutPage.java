package pages.ilmsPages.passwordManager;

import core_Base.BaseClass;
import core_Base.extentReport.Reporting;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import tools.CommonLibsUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class LogOutPage extends BaseClass {

    static CommonLibsUtils commonLibsUtils;

    public LogOutPage(BrowserType browserType,WebDriver driver) {
        super(browserType,driver);
        PageFactory.initElements(this.getWebDriver(),this);
    }


    public static By logOutProManager() throws InterruptedException, IOException {
        commonLibsUtils = new CommonLibsUtils(baseClass.getWebDriver());

        WebElement id = baseClass.getWebDriver().findElement(By.xpath("//*[@id='usacc_id_span']"));
        commonLibsUtils.waitUntilElementVisible(baseClass.getWebDriver(), id, 50000);
        CommonLibsUtils.pause(2000);
        commonLibsUtils.scrollToElement(baseClass.getWebDriver(), id);
        id.click();

        baseClass.getWebDriver().manage().timeouts().implicitlyWait(pageLoadTimeout, TimeUnit.SECONDS);
        Reporting.stepPassedWithScreenShot(id.getText());
        return By.linkText("Logout");
    }
}
