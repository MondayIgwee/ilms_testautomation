package pages.ilmsPages.passwordManager;

import core_Base.BaseClass;
import core_Base.SeleniumDriverFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import tools.CommonLibsUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class LoginPage extends BaseClass {

    public enum LOGONTO {ICEALION, ICEA_UG2, LOCAL_AUTHENTICATION}

    public static CommonLibsUtils commonLibsUtils;

    public LoginPage(BrowserType browserType, WebDriver driver) {
        super(browserType,driver);
        PageFactory.initElements(this.getWebDriver(),this);
    }

    public static String proManagerUrl() throws IOException, InterruptedException {
        return BaseClass.init();
    }

    public static By searchLoginUsername() throws InterruptedException {
        CommonLibsUtils.pause(2000);
        return By.cssSelector("input[id=username]");
    }

    public static By searchLoginPassword() throws InterruptedException {
        CommonLibsUtils.pause(2000);
        return By.cssSelector("input[id=password]");
    }

    public static By searchBtn() throws InterruptedException {
        commonLibsUtils = new CommonLibsUtils(baseClass.getWebDriver());

        int count = 0;
        while (true) {
            try {
                WebElement domainName = baseClass.getWebDriver().findElement(By.xpath("//*[@id='pmp_domainName']"));
                CommonLibsUtils.pause(2000);

                baseClass.getWebDriver().manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

                Select logOnto = new Select(domainName);

                if (domainName != null) {
                    logOnto.selectByVisibleText(LOGONTO.ICEALION.name().toUpperCase());
                    CommonLibsUtils.pause(2000);
                } else {
                    System.err.println("[FAILED - to click, please verify element]");
                }

                break;
            } catch (InvalidElementStateException e) {
                CommonLibsUtils.pause(1000);
                if (++count == maxTries) throw e;
            } catch (StaleElementReferenceException e) {
                System.out.println("Stale :" + count);
                CommonLibsUtils.pause(1000);
                if (++count == maxTries) throw e;
            } catch (WebDriverException e) {
                CommonLibsUtils.pause(1000);
                if (++count == maxTries) throw e;
            } catch (java.util.NoSuchElementException e) {
                CommonLibsUtils.pause(1000);
                if (++count == maxTries) throw e;
            }

        }

        return By.xpath("//*[@id='two_factor_form']/button/span/em[text()='Login']");
    }


    public static By validateSearchText(String term) {
        return By.xpath("//*[text()='" + term + "']");
    }

}
