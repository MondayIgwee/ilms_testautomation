package actions.appiumSteps;

import core_Base.AppiumDriverFactory;
import core_Base.extentReport.Reporting;
import org.openqa.selenium.remote.DesiredCapabilities;
import pages.appiumPages.EricBankPageObjects;

import java.io.IOException;
import java.net.MalformedURLException;

public class EricBankLoginActions extends AppiumDriverFactory {

    public EricBankLoginActions(DesiredCapabilities caps) throws MalformedURLException {
        super(caps);
    }

    /**
     *
     * @param user
     * @param pass
     * @return
     */
    public static String login(String user,String pass) throws InterruptedException, IOException {
         AppiumDriverFactory.enterText(EricBankPageObjects.username(),user);

        AppiumDriverFactory.enterText(EricBankPageObjects.password(),pass);


        AppiumDriverFactory.clickElement(EricBankPageObjects.login());

        return Reporting.finalisedTest();
    }

    public static String loginn(String user,String pass) throws InterruptedException {
        if (!AppiumDriverFactory.enterText(EricBankPageObjects.username(),user))
            return Reporting.testFailed("Failed to enter username");

        if (!AppiumDriverFactory.enterText(EricBankPageObjects.password(),pass))
            return Reporting.testFailed("Failed to enter password");


        if (!AppiumDriverFactory.clickElement(EricBankPageObjects.login()))
            return Reporting.testFailed("Failed to click login ");

        return Reporting.finalisedTest();
    }
}
