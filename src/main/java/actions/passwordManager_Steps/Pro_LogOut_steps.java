package actions.passwordManager_Steps;

import core_Base.BaseClass;
import core_Base.extentReport.Reporting;
import pages.ilmsPages.passwordManager.LogOutPage;

import java.io.IOException;

public class Pro_LogOut_steps extends BaseClass {


    public Pro_LogOut_steps(BrowserType browserType) {
        super(browserType,webDriver);
    }

    public static String logonProManager() throws IOException, InterruptedException {
        baseClass.clickElement(LogOutPage.logOutProManager());
        return Reporting.finalisedTest();
    }

}
