package actions.passwordManager_Steps;

import core_Base.BaseClass;
import core_Base.extentReport.Reporting;
import pages.ilmsPages.passwordManager.PasswordMainViewPage;
import java.io.IOException;
import java.util.Objects;

public class PasswordMainView_steps extends BaseClass {


    public PasswordMainView_steps(BrowserType browserType) {
        super(browserType,webDriver);
    }

    public static String connectToVirtualMachineProManager() throws IOException, InterruptedException {
          baseClass.clickWebElement(Objects.requireNonNull(PasswordMainViewPage.clickConnections()));
          baseClass.clickWebElement(Objects.requireNonNull(PasswordMainViewPage.clickVirtualMachine()));
          baseClass.clickWebElement(Objects.requireNonNull(PasswordMainViewPage.autoLogonFullView()));
          PasswordMainViewPage.switchTabs();
         //PasswordMainViewPage.clickConnections();
        return Reporting.finalisedTest();
    }
}
