package actions.passwordManager_Steps;

import core_Base.BaseClass;
import core_Base.extentReport.Reporting;
import org.junit.Assert;
import pages.ilmsPages.passwordManager.LoginPage;
import pages.ilmsPages.passwordManager.PasswordMainViewPage;
import tools.CommonLibsUtils;
import tools.Env_PropertiesUtils;

import java.io.IOException;

public class Pro_Login_steps extends BaseClass {

    public Pro_Login_steps(BrowserType browserType) {
        super(browserType,webDriver);

    }


    public static PasswordMainViewPage openPasswordManagerUrl() throws IOException, InterruptedException {
        CommonLibsUtils.startThread();
        Env_PropertiesUtils.getPropValues();
        baseClass.navigate(LoginPage.proManagerUrl());

        if (Env_PropertiesUtils.user.equals(Env_PropertiesUtils.user)) {
            baseClass.enterText(LoginPage.searchLoginUsername(), "" + Env_PropertiesUtils.user + "".trim());
            Reporting.stepPassedWithScreenShot("User Successfully enter username");

        } else {
             Reporting.testFailed("Failed to enter username");
        }

        if (Env_PropertiesUtils.pass.equals(Env_PropertiesUtils.pass)) {
            baseClass.enterText(LoginPage.searchLoginPassword(), "" + Env_PropertiesUtils.pass + "".trim());
            Reporting.stepPassedWithScreenShot("User Successfully enter password");

        } else {
             Reporting.testFailed("Failed to enter password");
        }

        baseClass.clickElement(LoginPage.searchBtn());
        Reporting.stepPassedWithScreenShot("clicked login button");
        Reporting.finalisedTest();
        return new PasswordMainViewPage(BrowserType.CHROME,webDriver);
    }
}
