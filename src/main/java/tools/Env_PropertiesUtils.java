package tools;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Env_PropertiesUtils {

    public static String chromeDriverPath;
    public static String user;
    public static String pass;

    public static String passTrixMain;
    static FileReader fileReader;
    static Properties prop;


    public static String getPropValues() throws IOException {
        try {
            prop = new Properties();
            String propFileName = "./src/main/resources/config.properties";
            fileReader = new FileReader(propFileName);
            prop.load(fileReader);

            passTrixMain = prop.getProperty("PassTrixMain");
            user = prop.getProperty("Username");
            pass = prop.getProperty("Password");

            fileReader.close();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {

        }
        return "";
    }
    public static String getPassTrixMain() {
        return passTrixMain;
    }
}
