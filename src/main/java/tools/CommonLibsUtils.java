package tools;

import core_Base.BaseClass;
import core_Base.SeleniumDriverFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.function.Function;

public class CommonLibsUtils {
    static WebDriver driver;

    public CommonLibsUtils(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement webDriverWait(WebElement ele, String text) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        ele = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(text)));
        return ele;
    }

    public WebElement waitUntilElementVisible(WebDriver driver, WebElement element, int delay) {
        try{
            WebDriverWait wait = new WebDriverWait(driver, delay);
            return wait.until(ExpectedConditions.visibilityOf(element));
        }catch (NoSuchElementException e){
            throw new RuntimeException("[FAILED] - Web element not visible within given time" + element +" Time "+ delay);
        }
    }
    public WebElement setFluentWait(WebElement ele,String xpathLocator) throws InterruptedException {
        //Waiting 30 seconds for an element to be present on the page, checking
        // for its presence once every 5 seconds.
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(3000))
                .pollingEvery(Duration.ofSeconds(3000))
                .ignoring(NoSuchElementException.class);

       ele  = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(By.xpath(xpathLocator));
            }
        });
        return  ele;
    }

    public void javaScriptExecutor_HighlightElement(WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style','border: 3px solid blue;');", ele);
    }

    public void javaScriptExecutor_HighlightElementByElement(By ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style','border: 3px solid blue;');", ele);
    }
    public void javaScriptExecutor_clickElement(WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", ele);
    }

    public void javaScriptExecuteAsyncScript_setTimeout(WebDriver driver, WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 10000);");
    }

    public void scrollToElement(WebDriver driver,WebElement ele) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);",ele);
    }

    public void actions(WebElement ele) {
        Actions ac = new Actions(driver);
        ac.moveToElement(ele).build().perform();
    }

    public static void pause(int mills) throws InterruptedException {
        Thread.sleep(mills);
    }
    public static void startThread(){
       BaseClass baseClassThread = new BaseClass(SeleniumDriverFactory.BrowserType.CHROME,driver);
        Thread thread = new Thread(baseClassThread);
        thread.start();
    }

}
