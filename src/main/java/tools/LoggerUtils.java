package tools;

import core_Base.BaseClass;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerUtils {

    public final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void getLogger(){
        try {
            FileHandler fileHandler = new FileHandler("myLogger.log");
            fileHandler.setEncoding("UTF-8");
            fileHandler.setLevel(Level.ALL);
            LOGGER.addHandler(fileHandler);
        }catch (Exception e){
            LOGGER.log(Level.SEVERE,"File logger not working ",e.getMessage());
        }
    }

}
