package tools;

import core_Base.BaseClass;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.io.File;

public class TakeScreenshotsUtils extends BaseClass {


    public TakeScreenshotsUtils(BrowserType browserType) {
        super(browserType,webDriver);
    }

    public static String takeScreenshots(boolean status)
    {
        //screenShotCounter++; //Increment screenshots
        StringBuilder imagePathBuilder = new StringBuilder();
        StringBuilder relativePathBuilder = new StringBuilder();

        try
        {
            imagePathBuilder.append(getReportingDirectory());
            relativePathBuilder.append("Screenshots/");
            new File(imagePathBuilder.toString()+(relativePathBuilder).toString()).mkdirs();

            screenShotCounter++; //Increment screenshots
            relativePathBuilder.append(screenShotCounter+"_");
            if (status)
            {
                relativePathBuilder.append("PASSED");
            } else {
                relativePathBuilder.append("FAILED");
            }
            relativePathBuilder.append(".png");

            File screenshot = ((TakesScreenshot)baseClass.getWebDriver()).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshot, new File(imagePathBuilder.append(relativePathBuilder).toString()));

            return "./"+relativePathBuilder.toString();
        } catch (Exception ex) {
            return null;
        }
    }
}
