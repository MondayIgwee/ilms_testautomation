package tools;

import core_Base.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WebElementUtils extends BaseClass {

    public WebElementUtils(BrowserType browserType) {
        super(browserType,webDriver);
    }

    public static WebElement getElementLocator(String locatorType, String locatorValue) {
        LoggerUtils.getLogger();
        WebElement element = null;

        switch (locatorType) {
            case "id":
                element = baseClass.getWebDriver().findElement(By.id(locatorValue));
                return element;
            case "cssSelector":
                element = baseClass.getWebDriver().findElement(By.cssSelector(locatorValue));
                return element;
            case "xpath":
                element = baseClass.getWebDriver().findElement(By.xpath(locatorValue));
                return element;
            case "name":
                element = baseClass.getWebDriver().findElement(By.name(locatorValue));
                return element;
            case "linkText":
                element = baseClass.getWebDriver().findElement(By.linkText(locatorValue));
                return element;
            case "partialLinkText":
                element = baseClass.getWebDriver().findElement(By.partialLinkText(locatorValue));
                return element;
            case "tagName":
                element = baseClass.getWebDriver().findElement(By.tagName(locatorValue));
                return element;
            case "className":
                element = baseClass.getWebDriver().findElement(By.className(locatorValue));
                return element;
            default:
                LoggerUtils.LOGGER.info("Incorrect locator or Type" + locatorType);
                break;
        }
        return null;
    }




    //Implementation
//       WebElement e =  WebElementUtils.getElementLocator("xpath","//div[@id='logo']/a/img[@class='img-responsive']");

    //******************************************************************
//   private <T extends By> WebElement getElement(Class<T> byType, String prop) {
//
//       if (byType.equals(ByClassName.class))
//           return getDriver().findElement(By.className(prop));
//       else if (byType.equals(ByCssSelector.class))
//           return getDriver().findElement(By.cssSelector(prop));
//       else if (byType.equals(ById.class))
//           return getDriver().findElement(By.id(prop));
//       else if (byType.equals(ByLinkText.class))
//           return getDriver().findElement(By.linkText(prop));
//       else if (byType.equals(ByName.class))
//           return getDriver().findElement(By.name(prop));
//       else if (byType.equals(ByPartialLinkText.class))
//           return getDriver().findElement(By.partialLinkText(prop));
//       else if (byType.equals(ByTagName.class))
//           return getDriver().findElement(By.tagName(prop));
//       else if (byType.equals(ByXPath.class))
//           return getDriver().findElement(By.xpath(prop));
//       else
//           throw new UnsupportedOperationException();
//   }
}
