package tools;

import core_Base.BaseClass;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Chrome_OptionsUtils {


    public static ChromeOptions getOptions() {
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("download.default_directory", BaseClass.getReportingDirectory() + File.separator + "src\\Test docs" + File.separator + "downloads");
        ChromeOptions options = new ChromeOptions();


        options.addArguments("start-maximized");
        options.addArguments("--disable-plugins");
        options.addArguments("version");
        options.addArguments("disable-extensions");
        options.addArguments("disable-popup-blocking");
        options.addArguments("incognito");
        options.addArguments("--block-new-web-contents");
        options.addArguments("no-sandbox");
        options.addArguments("--disable-gpu");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--ignore-ssl-errors=yes");
        options.addArguments("--ignore-certificate-errors");
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        options.setExperimentalOption("useAutomationExtension", false);
        options.setExperimentalOption("prefs", prefs);
        // driver = new ChromeDriver(options);
        // options.addExtensions(new File("/path/to/extension.crx"));
        // options.setBinary(new File(""));

        // For use with ChromeDriver:
        // driver = new ChromeDriver(options);

        // For use with RemoteWebDriver:
        // RemoteWebDriver remoteWebDriverriver = new RemoteWebDriver(
        //  new URL("http://localhost:4444/"),options);
        return options;
    }

}
