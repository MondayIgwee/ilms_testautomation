package excel_entities;

import core_Base.BaseClass;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ExcelReaderUtility extends BaseClass {


    public ExcelReaderUtility() {
        super(BrowserType.CHROME,webDriver);
    }

    public static XSSFWorkbook getWorkbook(String filePath) {
        try {
            FileInputStream excelFile = new FileInputStream(new File(filePath));
            XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
            return workbook;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage() + "\n...Could not get the workbook, Please verify excel file path..");

        }
        return null;
    }

    public static Sheet getWorkSheet(XSSFWorkbook workbook, String sheetName) {
        try {
            Sheet sheet = workbook.getSheet(sheetName);
            return sheet;
        } catch (Exception e) {
            System.err.println(e.getMessage() + "\n...Could not get the " + workbook + " or " + sheetName + ", Please verify excel file path..");
        }
        return null;
    }

    public static XSSFSheet getWorkSheett(String filePath, String sheetName) {
        try {

            XSSFWorkbook workbook = getWorkbook(filePath);
            XSSFSheet sheet = workbook.getSheet(sheetName);
            return sheet;
        } catch (Exception e) {
            System.err.println(e.getMessage() + "\n...Could not get the " + filePath + " or " + sheetName + ", Please verify excel file path..");
        }
        return null;
    }

    public static DataTable getSheetTable(String filePath, String sheetName) {
        try {
            DataTable table = new DataTable();
            LinkedList<DataRow> dataRows = new LinkedList<>();

            Sheet sheet = getWorkSheett(filePath, sheetName);
            Row firstRow = sheet.getRow(0);
            int lastColumn = firstRow.getLastCellNum();

            for (Row row : sheet) {
                DataRow currentRow = new DataRow();

                if (row.getRowNum() > 0 && !row.getCell(0).equals("")) {
                    for (int i = 0; i < lastColumn; i++) {
                        Cell currentCell = row.getCell(i);
                        DataColumn column;

                        if (currentCell == null) {
                            column = new DataColumn(firstRow.getCell(i).getStringCellValue(), "");

                        } else {
                            column = new DataColumn(firstRow.getCell(i).getStringCellValue(), currentCell.getStringCellValue());
                        }
                        currentRow.dataColumns.add(column);
                    }
                    // table.Ro
                }
            }
            return table;
        } catch (Exception ex) {
            System.err.println(ex.getMessage() + "\n...Could not get the " + filePath + " or " + sheetName + ", Please verify excel file path..");
        }
        return null;
    }


    public static List<DataRow> getData(String filepath, String sheetName) {
        try {
            XSSFSheet sheet = getWorkSheett(filepath, sheetName);
            List<DataRow> dataRows = new ArrayList<>();

            //Get Headers or Columns Row
            Row headerRow = sheet.getRow(0);
            List<String> headers = new ArrayList<>();

            for (int i = 0; i < headerRow.getLastCellNum(); i++) {
                Cell cell = headerRow.getCell(i);
                headers.add(cell.getStringCellValue());
            }

            //Get Rows
            for (Row row : sheet) {
                DataRow curRow = new DataRow();

                if (row.getRowNum() > 0 && !row.getCell(0).equals("")) {

                    for (int j = 1; j < row.getLastCellNum(); j++) {
                        Cell cell = row.getCell(j);

                        DataColumn curColumn = new DataColumn(headers.get(j), cell.getStringCellValue());
                        curRow.dataColumns.add(curColumn);
                    }
                    dataRows.add(curRow);
                }
            }
            return dataRows;
        } catch (Exception e) {
            return null;
        }
    }


}
