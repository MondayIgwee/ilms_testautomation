package excel_entities;

import java.util.LinkedList;
import java.util.function.Predicate;

public class DataRow {

   public LinkedList<DataColumn> dataColumns;

    public DataRow(){
        dataColumns = new LinkedList<>();
    }

    public  String getColumnValue(String columnHeader){
        try{
            Predicate<DataColumn> predicate = dataCol -> dataCol.getKey().equals(columnHeader);
            DataColumn obj = dataColumns.stream().filter(predicate).findFirst().get();
            return obj.getValue();
        }catch (Exception e){
            System.err.println("[ERROR] Could not find the column - "+ columnHeader +" - in table row");
        }
        return "";
    }

    public  String getColumn(String columnHeader){
        try{
            Predicate<DataColumn> predicate = dataCol -> dataCol.getKey().equals(columnHeader);
            DataColumn obj = dataColumns.stream().filter(predicate).findFirst().get();
            return obj.getValue();
        }catch (Exception e){
            System.err.println("[ERROR] Could not find the column - "+ columnHeader +" - in table row");
        }
        return "";
    }
}
