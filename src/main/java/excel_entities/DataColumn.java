package excel_entities;

public class DataColumn {

    /**
     * Create a Key Value pairs
     * for the Columns
     */

    public String key;  //column Header
    public String value; //column Value

    public DataColumn(String key,String value){
        this.key=key;
        this.value=value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
